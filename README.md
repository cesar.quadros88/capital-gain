## Requisitos para executar projeto
 Instalar JDK 17
 
## Como executar o projeto

Primeiramente, para baixar as dependencias do projeto e gerar o executavel, execute o seguinte comando na raiz do projeto:

```
./mvn clean install
```

Será criado a pasta ```target``` e dentro dela é gerado o arquivo chamado ```capital-gain-jar-with-dependencies.jar```

### Executando a aplicação
Após o passo acima, abra o terminal e vá até a pasta ```target``` e execute:

```
java -jar capital-gain-jar-with-dependencies.jar
```

 <br />
Nesse momento a aplicação será iniciada.
 <br />
Para realizar as operação, o JSON deverá estar apenas em uma linha, por exemplo:

```
[{"operation":"buy", "unit-cost":10.00, "quantity": 100},{"operation":"sell", "unit-cost":15.00, "quantity": 50},{"operation":"sell", "unit-cost":15.00, "quantity":50}]
```
Caso haja quebra de linha no JSON da lista de operação, o programa ira gerar uma exceção em tempo de execução, exemplo invalido:

```
[{"operation":"buy", "unit-cost":10.00, "quantity": 100},
{"operation":"sell", "unit-cost":15.00, "quantity": 50},
{"operation":"sell", "unit-cost":15.00, "quantity":50}]
```

### Executando mais de uma lista de operações
É possivel enviar mais de uma lista de operações ao mesmo tempo, porem, cada lista deverá estar em uma linha, e na ultima deve haver uma quebra de linha, exemplo:
```
[{"operation":"buy", "unit-cost":10.00, "quantity": 100},{"operation":"sell", "unit-cost":15.00, "quantity": 50},{"operation":"sell", "unit-cost":15.00, "quantity": 0}]
[{"operation":"buy", "unit-cost":10.00, "quantity": 10000},{"operation":"sell", "unit-cost":20.00, "quantity": 5000},{"operation":"sell", "unit-cost":5.00, "quantity": 5000}]

```
Note que após a segunda linha de operações, tem uma linha em branco. Dessa forma ambas as linhas serão executas de umas vez, tendo como saida o exemplo abaixo:
```
[{"tax":0},{"tax":0},{"tax":0}]
[{"tax":0},{"tax":10000.00},{"tax":0}]
```

### Executando testes
Para executar os testes, execute o seguinte comando na raiz do projeto

```
./mvn test
```


### Decisões técnicas
* Para a solução foi utilizado Java 17, linguagem que tenho dominio, tentei usar o minimo de framework possivel, apenas para tratamento do JSON e Testes.
  * junit
  * gson
* Foi utilizado o Pattern Strategy para a execução das logicas de compra/venda, visando novas features, por exemplo uma simulação de compra/venda, etc...
