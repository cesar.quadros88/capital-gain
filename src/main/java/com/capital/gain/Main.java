package com.capital.gain;

import com.capital.gain.service.CalculateService;
import com.capital.gain.domain.Operation;
import com.capital.gain.domain.ValuesOperationData;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()){
            final String operationString = scanner.nextLine();
            final Gson gson = new Gson();
            final List<Operation> operations = gson.fromJson(operationString, new TypeToken<List<Operation>>(){}.getType());
            final CalculateService calculateService = new CalculateService();
            final ValuesOperationData valuesOperationData = calculateService.calculate(operations);
            String rateJson = gson.toJson(valuesOperationData.getRate());
            System.out.println(rateJson);
        }
    }
}