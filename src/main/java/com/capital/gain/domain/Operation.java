package com.capital.gain.domain;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public record Operation(
    String operation,
    @SerializedName("unit-cost")
    BigDecimal unitCost,
    int quantity
    ) {
}
