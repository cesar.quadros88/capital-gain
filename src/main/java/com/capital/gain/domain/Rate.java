package com.capital.gain.domain;

import java.math.BigDecimal;

public record Rate(
    BigDecimal tax
) {
}
