package com.capital.gain.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class ValuesOperationData {
    private Integer numberCurrentShares;
    private BigDecimal currentWeightedAverage;
    private BigDecimal prejudice;
    private List<Rate> tax;

    public ValuesOperationData(final Integer numberCurrentShares, final BigDecimal currentWeightedAverage, final BigDecimal prejudice) {
        this.numberCurrentShares = numberCurrentShares;
        this.currentWeightedAverage = currentWeightedAverage;
        this.prejudice = prejudice;
        this.tax = new ArrayList<>();
    }

    public Integer getNumberCurrentShares() {
        return numberCurrentShares;
    }

    public void setNumberCurrentShares(final Integer numberCurrentShares) {
        this.numberCurrentShares = numberCurrentShares;
    }

    public BigDecimal getCurrentWeightedAverage() {
        return currentWeightedAverage;
    }

    public BigDecimal getPrejudice() {
        return prejudice;
    }

    public void setPrejudice(final BigDecimal prejudice) {
        this.prejudice = prejudice;
    }

    public List<Rate> getRate() {
        return tax;
    }

    public void setRate(final Rate rate) {
        this.tax.add(rate);
    }

    public void calculateCurrentWeightedAverage(final Operation operation) {
        currentWeightedAverage =
            BigDecimal.valueOf(numberCurrentShares)
            .multiply(currentWeightedAverage)
            .add(BigDecimal.valueOf(operation.quantity())
                .multiply(operation.unitCost()))
            .divide(BigDecimal.valueOf(numberCurrentShares).add(BigDecimal.valueOf(operation.quantity())), 2, RoundingMode.HALF_UP);
    }

    public void deductSale(final Operation operation){
        numberCurrentShares -= operation.quantity();
    }
}
