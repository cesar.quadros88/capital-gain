package com.capital.gain.domain.type;

import com.capital.gain.service.factory.Calculate;
import com.capital.gain.service.factory.impl.BuyCalculate;
import com.capital.gain.service.factory.impl.SellCalculate;

public enum OperationType {
    BUY(new BuyCalculate()),
    SELL(new SellCalculate());

    private Calculate calculate;

    OperationType(final Calculate calculate){
        this.calculate = calculate;
    }

    public Calculate getCalculate(){
        return this.calculate;
    }
}
