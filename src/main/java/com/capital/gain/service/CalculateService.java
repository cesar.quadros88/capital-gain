package com.capital.gain.service;

import com.capital.gain.domain.Operation;
import com.capital.gain.domain.type.OperationType;
import com.capital.gain.domain.ValuesOperationData;

import java.math.BigDecimal;
import java.util.List;

public class CalculateService {

    public ValuesOperationData calculate(List<Operation> operations){
        final ValuesOperationData valuesOperationData = new ValuesOperationData(0, BigDecimal.ZERO,BigDecimal.ZERO);
        operations.forEach(operation -> {
            final OperationType operationType = OperationType.valueOf(operation.operation().toUpperCase());
            operationType.getCalculate().calculate(operation, valuesOperationData);
        });
        return valuesOperationData;
    }
}
