package com.capital.gain.service.factory;

import com.capital.gain.domain.Operation;
import com.capital.gain.domain.ValuesOperationData;

public interface Calculate {
    void calculate(final Operation operation, final ValuesOperationData valuesOperationData);
}
