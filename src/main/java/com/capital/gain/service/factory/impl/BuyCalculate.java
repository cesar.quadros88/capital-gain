package com.capital.gain.service.factory.impl;


import com.capital.gain.domain.Operation;
import com.capital.gain.domain.Rate;
import com.capital.gain.service.factory.Calculate;
import com.capital.gain.domain.ValuesOperationData;

import java.math.BigDecimal;

public class BuyCalculate implements Calculate {
    @Override
    public void calculate(final Operation operation, final ValuesOperationData valuesOperationData) {
        valuesOperationData.calculateCurrentWeightedAverage(operation);
        valuesOperationData.setNumberCurrentShares(valuesOperationData.getNumberCurrentShares()+operation.quantity());
        valuesOperationData.setRate(new Rate(BigDecimal.ZERO));
    }
}
