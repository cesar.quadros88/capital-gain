package com.capital.gain.service.factory.impl;

import com.capital.gain.domain.Operation;
import com.capital.gain.domain.Rate;
import com.capital.gain.service.factory.Calculate;
import com.capital.gain.domain.ValuesOperationData;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SellCalculate implements Calculate {

    public static final double PERCENTAGE_RATE = 0.2;
    private static final BigDecimal LIMIT_TAXED_OPERATION = new BigDecimal(20000);

    @Override
    public void calculate(final Operation operation, final ValuesOperationData valuesOperationData) {
        valuesOperationData.deductSale(operation);
        final BigDecimal quantityBigDecimal = BigDecimal.valueOf(operation.quantity());
        if (isPrejudice(operation, valuesOperationData)){
            final BigDecimal prejudice = getPrejudice(operation, valuesOperationData, quantityBigDecimal);
            valuesOperationData.setPrejudice(prejudice);
            valuesOperationData.setRate(new Rate(BigDecimal.ZERO));
            return;
        }
        BigDecimal gain = BigDecimal.ZERO;
        final BigDecimal totalOperation = operation.unitCost().multiply(quantityBigDecimal);
        final BigDecimal profitTransaction = operation.unitCost().subtract(
            valuesOperationData.getCurrentWeightedAverage()).multiply(quantityBigDecimal);

        BigDecimal actualPrejudice = valuesOperationData.getPrejudice().subtract(profitTransaction);

        if(actualPrejudice.compareTo(BigDecimal.ZERO) < 0){
            gain = actualPrejudice.negate();
            actualPrejudice = BigDecimal.ZERO;
        }
        valuesOperationData.setRate(new Rate(getRate(totalOperation,gain)));
        valuesOperationData.setPrejudice(actualPrejudice);
    }

    private static BigDecimal getPrejudice(final Operation operation, final ValuesOperationData valuesOperationData, final BigDecimal quantityBigDecimal) {
        return valuesOperationData.getCurrentWeightedAverage()
            .subtract(operation.unitCost())
            .multiply(quantityBigDecimal);
    }

    private boolean isPrejudice(final Operation operation, final ValuesOperationData valuesOperationData){
        return operation.unitCost().compareTo(valuesOperationData.getCurrentWeightedAverage()) < 0;
    }

    private boolean isTaxed(final BigDecimal totalOperation){
        return totalOperation.compareTo(LIMIT_TAXED_OPERATION) > 0;
    }

    private BigDecimal getRate(final BigDecimal totalOperation, final BigDecimal gain){
        if (isTaxed(totalOperation) && gain.compareTo(BigDecimal.ZERO) > 0){
            return gain.multiply(BigDecimal.valueOf(PERCENTAGE_RATE)).setScale(2, RoundingMode.HALF_UP);
        }
        return BigDecimal.ZERO;
    }
}
