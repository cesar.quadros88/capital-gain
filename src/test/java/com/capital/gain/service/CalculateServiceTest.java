package com.capital.gain.service;


import com.capital.gain.domain.Operation;
import com.capital.gain.domain.Rate;
import com.capital.gain.domain.ValuesOperationData;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.List;

public class CalculateServiceTest {

    @Test
    public void test_case_1(){
        final CalculateService calculateService = new CalculateService();
        final Operation op1 = new Operation("buy",new BigDecimal(10),100);
        final Operation op2 = new Operation("sell",new BigDecimal(15),50);
        final Operation op3 = new Operation("sell",new BigDecimal(15),50);
        final ValuesOperationData actualResponse = calculateService.calculate(List.of(op1, op2, op3));
        final List<Rate> rateActual = actualResponse.getRate();

        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(0).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(1).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(2).tax());
    }

    @Test
    public void test_case_2(){
        final CalculateService calculateService = new CalculateService();
        final Operation op1 = new Operation("buy",new BigDecimal(10),10000);
        final Operation op2 = new Operation("sell",new BigDecimal(20),5000);
        final Operation op3 = new Operation("sell",new BigDecimal(5),5000);
        final ValuesOperationData actualResponse = calculateService.calculate(List.of(op1, op2, op3));
        final List<Rate> rateActual = actualResponse.getRate();

        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(0).tax());
        Assert.assertEquals(new BigDecimal(10000.00).setScale(2), rateActual.get(1).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(2).tax());
    }

    @Test
    public void test_case_3(){
        final CalculateService calculateService = new CalculateService();
        final Operation op1 = new Operation("buy",new BigDecimal(10),10000);
        final Operation op2 = new Operation("sell",new BigDecimal(5),5000);
        final Operation op3 = new Operation("sell",new BigDecimal(20),3000);
        final ValuesOperationData actualResponse = calculateService.calculate(List.of(op1, op2, op3));
        final List<Rate> rateActual = actualResponse.getRate();

        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(0).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(1).tax());
        Assert.assertEquals(new BigDecimal(1000.00).setScale(2), rateActual.get(2).tax());
    }

    @Test
    public void test_case_4(){
        final CalculateService calculateService = new CalculateService();
        final Operation op1 = new Operation("buy",new BigDecimal(10),10000);
        final Operation op2 = new Operation("buy",new BigDecimal(25),5000);
        final Operation op3 = new Operation("sell",new BigDecimal(15),10000);
        final ValuesOperationData actualResponse = calculateService.calculate(List.of(op1, op2, op3));
        final List<Rate> rateActual = actualResponse.getRate();

        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(0).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(1).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(2).tax());
    }

    @Test
    public void test_case_5(){
        final CalculateService calculateService = new CalculateService();
        final Operation op1 = new Operation("buy",new BigDecimal(10),10000);
        final Operation op2 = new Operation("buy",new BigDecimal(25),5000);
        final Operation op3 = new Operation("sell",new BigDecimal(15),10000);
        final Operation op4 = new Operation("sell",new BigDecimal(25),5000);

        final ValuesOperationData actualResponse = calculateService.calculate(List.of(op1, op2, op3, op4));
        final List<Rate> rateActual = actualResponse.getRate();

        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(0).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(1).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(2).tax());
        Assert.assertEquals(new BigDecimal(10000).setScale(2), rateActual.get(3).tax());
    }

    @Test
    public void test_case_6(){
        final CalculateService calculateService = new CalculateService();
        final Operation op1 = new Operation("buy",new BigDecimal(10),10000);
        final Operation op2 = new Operation("sell",new BigDecimal(2),5000);
        final Operation op3 = new Operation("sell",new BigDecimal(20),2000);
        final Operation op4 = new Operation("sell",new BigDecimal(20),2000);
        final Operation op5 = new Operation("sell",new BigDecimal(25),1000);

        final ValuesOperationData actualResponse = calculateService.calculate(List.of(op1, op2, op3, op4, op5));
        final List<Rate> rateActual = actualResponse.getRate();

        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(0).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(1).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(2).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(3).tax());
        Assert.assertEquals(new BigDecimal(3000).setScale(2), rateActual.get(4).tax());
    }

    @Test
    public void test_case_7(){
        final CalculateService calculateService = new CalculateService();
        final Operation op1 = new Operation("buy",new BigDecimal(10),10000);
        final Operation op2 = new Operation("sell",new BigDecimal(2),5000);
        final Operation op3 = new Operation("sell",new BigDecimal(20),2000);
        final Operation op4 = new Operation("sell",new BigDecimal(20),2000);
        final Operation op5 = new Operation("sell",new BigDecimal(25),1000);
        final Operation op6 = new Operation("buy",new BigDecimal(20),10000);
        final Operation op7 = new Operation("sell",new BigDecimal(15),5000);
        final Operation op8 = new Operation("sell",new BigDecimal(30),4350);
        final Operation op9 = new Operation("sell",new BigDecimal(30),650);


        final ValuesOperationData actualResponse = calculateService.calculate(List.of(op1, op2, op3, op4, op5, op6, op7, op8, op9));
        final List<Rate> rateActual = actualResponse.getRate();

        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(0).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(1).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(2).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(3).tax());
        Assert.assertEquals(new BigDecimal(3000).setScale(2), rateActual.get(4).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(5).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(6).tax());
        Assert.assertEquals(new BigDecimal(3700).setScale(2), rateActual.get(7).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(8).tax());
    }

    @Test
    public void test_case_8(){
        final CalculateService calculateService = new CalculateService();
        final Operation op1 = new Operation("buy",new BigDecimal(10),10000);
        final Operation op2 = new Operation("sell",new BigDecimal(50),10000);
        final Operation op3 = new Operation("buy",new BigDecimal(20),10000);
        final Operation op4 = new Operation("sell",new BigDecimal(50),10000);
        final ValuesOperationData actualResponse = calculateService.calculate(List.of(op1, op2, op3, op4));
        final List<Rate> rateActual = actualResponse.getRate();

        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(0).tax());
        Assert.assertEquals(new BigDecimal(80000).setScale(2), rateActual.get(1).tax());
        Assert.assertEquals(BigDecimal.ZERO, rateActual.get(2).tax());
        Assert.assertEquals(new BigDecimal(60000).setScale(2), rateActual.get(3).tax());
    }
}